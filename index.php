<?php
require_once 'Interface/WritableInterface.php';
require_once 'Class/Item.php';
require_once 'Class/Food.php';
require_once 'Class/GoodsItem.php';
require_once 'Class/Bonus.php';
require_once 'Class/ItemsWriter.php';

//create object
$objectFood = new Food('BigMack',90);
$objectGoodsItem = new GoodsItem('Mackbook',30000,1500);
$objectBonus = new Bonus('Mouse','goods','Mouse for laptop');

$itemWriter = new ItemsWriter();

$itemWriter->addItem($objectFood);
$itemWriter->addItem($objectGoodsItem);
$itemWriter->addItem($objectBonus);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<!--Content block start-->
<div style="text-align: center;">
<table class="table">
    <thead class="thead-light">
    <tr>
        <th width="200" scope="col">Title</th>
        <th width="200" scope="col">Type</th>
        <th width="200" scope="col">Price</th>
        <th width="200" scope="col">Description</th>
    </tr>
    </thead>
    <tbody>
        <?=$itemWriter->write();?>
    </tbody>
</table>
</div>
<!--Content block end-->

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->

</body>
</html>
