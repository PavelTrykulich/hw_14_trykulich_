<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 13.05.2018
 * Time: 12:33
 */

class Bonus implements Writable
{
    protected $title = ' ';
    protected $type = ' ';
    protected $description = ' ';

    public function __construct($title,$type,$description)
    {
        $this->title = $title;
        $this->type = $type;
        $this->description = $description;
    }

    public function getSummaryLine()
    {
        $str = ' ';
        $str .= '<td>' . $this->title . '</td>';
        $str .= '<td>' . self::getType() . '</td>';
        $str .= '<td>' . 'is free' .  '</td>';
        $str .= '<td>' . $this->description . '</td>';
        return $str;
    }

    static public function getType()
    {
        return 'bonus';
    }

}