<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 13.05.2018
 * Time: 12:19
 */

class GoodsItem extends Item
{
    protected $discount = 0;

    public function __construct($title, $price, $discount)
    {
        parent::__construct($title, $price);
        $this->discount = $discount;
    }

    public function getPrice()
    {
        return $this->price - $this->discount;
    }

    static public function getType()
    {
        return 'goods';
    }

    public function getSummaryLine ()
    {
        $str = ' ';
        $str .= '<td>' . $this->title . '</td>';
        $str .= '<td>' . static::getType() . '</td>';
        $str .= '<td>' . $this->getPrice() . '</td>';
        return $str;
    }
}